/**
 * Adds event listener to edit-size-container and close-overlay
 *
 * toggling the visibility of the edit overlay view
 */

document.getElementById("edit-size-container")
    .addEventListener("click", toggleEditOverlay, true);

document.getElementById("close-overlay")
    .addEventListener("click", toggleEditOverlay, true);

function toggleEditOverlay() {
    let overlayClasses = document.getElementById("edit-overlay").classList;

    if (overlayClasses.contains("show")) {
        overlayClasses.remove("show");
    }
    else {
        overlayClasses.add("show");
    }
}
/**
 *  Add click event to the rotate button.
 *  Toggles image 90 degrees and back if already rotated
 */

document.getElementById("rotate-button")
    .addEventListener("click", rotateImage, true);

function rotateImage() {
    let userImageClassList = document.getElementById("user-image").classList;
    if (userImageClassList.contains('rotate')) {
        userImageClassList.remove('rotate');
    }
    else {
        userImageClassList.add('rotate');
    }
}

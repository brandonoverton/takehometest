/**
 * Adds "click" event listener to the menu button
 *
 * toggles the dropdown
 *
 * event listener added to body when menu is visible and removed when it closes
 */

document.getElementById("menu-button").addEventListener("click", toggleMenu, true);

function toggleMenu(event) {
    event.stopPropagation();

    console.log('target', event.target);
    if (event.target.classList.contains("menu-item")) {
        return;
    }
    console.log("here");
    let menuClassList = document.getElementById("menu-container").classList;
    if (menuClassList.contains('show')) {
        closeMenu();
    }
    else {
        document.body.addEventListener('click', closeMenu);
        menuClassList.add('show');
    }
}

function closeMenu() {
    let menuClassList = document.getElementById("menu-container").classList;
    document.body.removeEventListener('click', closeMenu);
    menuClassList.remove('show');
}